###### Author: Rui Cunha
###### Email: r.cunha@datagen.eu

### About this class
It provides a logging engine to a database.

###### The database
A method is provided to create the database structure, you need to run it first. The log table is not indexed. This is because it is supposed to be a temporary stack of records, that you will need to collect later according to your own needs. Indexing takes a heavy toll on performance, and logging is quite an intensive task. You dont want to do that when your users are waiting for a response.

###### How it works
Logs are stacked on the logs table. If for some reason an error occurs and they cannot be written to the database, a text file is created for each log on your filesystem. You need to include a periodic task in your system that runs the method importLogsFromTextFilesToDatabase(). This will move everything from the filesystem to your log table. Be aware that depending on how many files to process, this can be very resource intensive. Do it in a way that does not compromise performance to your users.
The power of logging comes from analytics. This is out of the scope of this class as it is built according to each one's specific needs. From time to time, move the logs from the logs table to your own analytics system. Again, remember this is resource intensive. Do it conscientiously. 

###### How to set it up
1. Include the file logs.php, if you are not using composer. If you are using composer, just type:
```shell
composer require hotworks/logs
```
2. Create the database. You have a method for this in the class:
```php
$log=new Logs( $myDatabaseConnectionObject );
$log->createDatabaseStructure();
```
3. Use it.

###### How to use it
1. Instatiate the class providing the database connection, which needs to be a PDO object.
```php
$log=new Logs( $myDatabaseConnectionObject );
```
2. Provide the data and write it.
```php
$log->title="Invalid login attempt";
$log->description="Someone failed to provide a correct username and password";
$log->write();
```
3. From time to time import any text file that may have been written with the method importLogsFromTextFilesToDatabase();
4. Logs are useless (and even undesirable) if you are not going to process them. From time to time move the stack to another system, where performance is not a user problem. The frequency of this should depend on how intensive your logging is. The more you acummulate, the unperformant your system will be during the moving process. On the destination system, index and query at your needs. Just dont do it on the system that is activelly logging.

###### Hire a programmer
I may provide consultancy on this, if you need.